
credit0=[[# Star Phase #

Developer:
Ricky Thomson (aka Jigoku)
Source: https://github.com/Jigoku/starphase/tree/master/src

Forked by 'Glitchapp'
Source:		https://codeberg.org/glitchapp/Starphase

Assets:
licensed under CC-BY-SA 3.0

by Ricky K Thomson
Source      : http://github.com/jigoku

Portraits: 
Jorhlok
https://opengameart.org/content/mostly-16x18-characters-and-48x48-portraits-repack


Sound effects:

Explosions:

muffled-distant
by NenadSimic
Source      : http://opengameart.org/content/muffled-distant-explosion

Explosions set
by Unnamed (Viktor.Hahn@web.de)

intercom.ogg
by Johan Brodd
Source      : https://opengameart.org/content/space-ships-in-distress

Menu sfx:
by mobeyee.com
Source      : https://opengameart.org/content/cloud-click

option.ogg
by mobeyee.com
Source      : https://opengameart.org/content/metal-click

select.ogg
by mobeyee.com
Source      : https://opengameart.org/content/cloud-click

Pickups sound:
by Ricky K Thomson
Source      : http://github.com/jigoku

Projectiles
by Ricky K Thomson
Source      : http://github.com/jigoku


Music:

overdrive.ogg
by Bogart VGM (https://www.facebook.com/BogartVGM/)
Source      : https://opengameart.org/content/maximum-overdrive

conentration.ogg
by Bogart VGM (https://www.facebook.com/BogartVGM/)
Source      : https://opengameart.org/content/scifi-concentration

neoncity.ogg
by Bogart VGM (https://www.facebook.com/BogartVGM/)
Source      : https://opengameart.org/content/neon-city

ambient.ogg
by brandon75689
Source      : https://opengameart.org/content/tragic-ambient-main-menu

ix.ogg
by cinameng
Source      : https://opengameart.org/content/ix

cybermonk.ogg,
onebigboss.ogg,
& roundone.ogg
by iamonabe 
https://opengameart.org/users/iamoneabe

deprecation.ogg
inevitable.ogg
mediathreat.ogg
by maxstack
Source      : http://opengameart.org/content/endgame-singularity

crystal-space.ogg
bazaarnet.ogg
the-client.ogg
by maxstack
Source      : http://opengameart.org/content/freelance

nebula.ogg
by maxstack
Source      : http://opengameart.org/content/nebula

through-space.ogg
by maxstack
Source      : http://opengameart.org/content/through-space

thrust.ogg
by Music by Matthew Pablo http://www.matthewpablo.com
Source      : https://opengameart.org/content/thrust-sequence

dimensions.ogg
by Music by Matthew Pablo http://www.matthewpablo.com
Source      : https://opengameart.org/content/space-dimensions-8bitretro-version

persuit.ogg
by Music by Matthew Pablo http://www.matthewpablo.com
Source      : https://opengameart.org/content/tactical-pursuit

vilified.ogg
by Music by Matthew Pablo http://www.matthewpablo.com
Source      : https://opengameart.org/content/vilified

evasion.ogg
by Music by Matthew Pablo http://www.matthewpablo.com
Source      : https://opengameart.org/content/evasion

railjet.ogg
by Deceased Superior Technician, http://www.nosoapradio.us
Source      : https://opengameart.org/content/railjet-long-seamless-loop

space.ogg
by Alexandr Zhelanov
Source      : http://opengameart.org/content/space-1

License
GNU GPLv3 / CC-BY-SA
]]

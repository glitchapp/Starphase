cutscene1Loaded=false
Cutscene1Played=false 
Cutscene1Playing=false
Boss1Destroyed=false
cutScene1Duration=38
counter=0
ShieldLowScenePlaying=false
ShieldLowScenePlayed=false
ShieldLowSceneLoade=false


function loadCutscene1()
	local videoPath = "data/videos/cutscenes/Cutscene1Video.ogv"
	local audioPath = "data/videos/cutscenes/Cutscene1Audio.ogg"
	local dialogsPath = "data/videos/cutscenes/Cutscene1Dialogs.ogg"
	local imagePath = "data/videos/cutscenes/Cutscene1Image.png" -- Fallback image path

	if love.filesystem.getInfo(videoPath) then
		cutscene1Video = love.graphics.newVideo(videoPath)
		cutscene1Audio = love.audio.newSource(audioPath, "stream")
		cutscene1Dialogs = love.audio.newSource(dialogsPath, "stream")
	else
		cutscene1Video = love.graphics.newImage(imagePath) -- Use image instead of video
		cutscene1Audio = nil -- No audio if the video is missing
		cutscene1Dialogs = nil -- no dialogs if the video is missing
	end

	cutscene1Loaded = true

	love.audio.stop()
end

function loadShieldLowCutscene()
	if ShieldLowSceneLoaded==false and (Cutscene1Played==false) and (cutsceneShieldLowVideo==nil) then
		local shieldLowVideoPath = "data/videos/cutscenes/ShieldLow/shieldLow.ogv"
	
	
	if love.filesystem.getInfo(shieldLowVideoPath) then
		cutsceneShieldLowVideo = love.graphics.newVideo(shieldLowVideoPath)
		ShieldLowSceneLoaded=true
		--cutscene1Audio = love.audio.newSource(audioPath, "stream")
		print("Shield low cutscene loaded.")
	else
		cutsceneShieldLowVideo = nil -- Use image instead of video
		cutsceneShieldLowAudio = nil -- No audio if the video is missing
		print("Shield low cutscene video not found.")
		Cutscene1Played=true
		ShieldLowScenePlaying=false
	end
	end
	

end

function updateShieldLowCutscene(dt)
	 if ShieldLowScenePlaying==true then
        counter = counter + dt
        print("Shield low cutscene counter:", counter)
        
        if counter > 4 then
            ShieldLowScenePlaying = false
            ShieldLowScenePlayed = true
            counter = 0
            print("Shield low cutscene finished.")
        end
    end
end



function drawShieldLowCutscene()
	
  if cutsceneShieldLowVideo and ShieldLowScenePlaying then
		cutsceneShieldLowVideo:play()
        --love.graphics.draw(cutsceneShieldLowVideo, 0, 0, 0, 0.6)
        love.graphics.draw(cutsceneShieldLowVideo,game.width/2,game.height-300,0,0.1)
    end
	
end

function updateCutscene1(dt)
--print(counter)
	counter=counter+dt
	if cutscene1Loaded==true and Cutscene1Played==false and Boss1Destroyed==true and counter>48 then
		--restore parameters
		Cutscene1Playing=false
		cutscene1Loaded=false
		Cutscene1Played=false 
		Boss1Destroyed=false
		endCredits = true
		counter=0
		sound:playbgm(24)
	end
	
end


function playCutscene1()
	if cutscene1Loaded == true and Cutscene1Played == false and Boss1Destroyed == true then
		love.graphics.setColor(1, 1, 1, 1)
		if not (counter>25) then
		-- Play cutscene
		if cutscene1Video:typeOf("Video") then
				love.graphics.draw(cutscene1Video, 0, 0, 0, 0.6)
				cutscene1Video:play()
			if cutscene1Audio then
					cutscene1Audio:play()
			end
			if cutscene1Dialogs and counter>14 then
				cutscene1Dialogs:play()
			end
		end
		else
			love.graphics.draw(cutscene1Video, 0, 0, 0, 0.6)
		end
	end
end


function playCreditsAfterEnd()
	
end

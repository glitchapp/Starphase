# Star Phase (Fork from https://github.com/Jigoku/starphase)

<img src="screenshot.webp" width=70% height=70%>

I'm not the original developer of this game, the original repository can be found on the link above.

I added to this fork:

- Thumbstick control support (You can navigate menus and play the game with the left thumbstick)
- Added touch controls and several tweaks to run on mobiles (check releases for more info).
- Love-wepb library (game size reduced from 272Mb to 138,7Mb, 50% less space needed)
- Two players mode (work in progress)
- Dialogs
- Boss (first stage)
- Credits


# Star Phase
A top down / side scrolling space shooter made with Lua and Love2D. 

This is an early work in progress, so many bugs / changes will come.

Reccomended to use 1920x1080 resolution for now. ([issue #9](https://github.com/Jigoku/starphase/issues/9))

### Running the game
Run the game from the src/ directory using
```
$ love src/
```

To create standalone executables for all platforms 
```
$ make && make all
```

### Requirements
Install the love2d library from here https://love2d.org

### Controls
See `binds.lua` for game controls

# License
GNU GPLv3 / CC-BY-SA

# Screenshots

![1](screenshots/1.jpg)

![2](screenshots/2.jpg)

![3](screenshots/3.jpg)

![4](screenshots/4.jpg)
